create database Trabalho_G2_Desktop_II;

use Trabalho_G2_Desktop_II;

create table clientes
(
	id_cliente int auto_increment primary key,
	nome_cliente varchar(45) not null,
    data_nascimento datetime not null,
    genero enum("Masculino", "Feminino") not null,
    endereco varchar(100) not null,
    saldo decimal(8,2) default 00.00
);