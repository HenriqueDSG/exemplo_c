﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{
    public partial class Cadastro_Novo : Form
    {

        MySqlConnection conexao;
        Clientes cliente = new Clientes();
        
        public Cadastro_Novo()
        {
            InitializeComponent();           
            conexao = new MySqlConnection("Server = localhost; Database = Trabalho_G2_Desktop_II; port = 3306; username = root; password = ");
            WindowState = FormWindowState.Maximized;
        }

        private void button_fechar_Click(object sender, EventArgs e)
        {
            DialogResult opcao = MessageBox.Show( "Deseja fechar a janela? ","Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (opcao == DialogResult.Yes)
            {
                Close();
            }
        }

        private void button_salvar_Click(object sender, EventArgs e)
        {            
            cliente.Nome_cliente = textBox_nome.Text;
            cliente.Genero = comboBox_genero.SelectedItem.ToString();
            cliente.Endereco = textBox_endereco.Text;
            cliente.Saldo = num_saldo.Value;      
            DateTime data_nascimento = date_data.Value;
            string data_formatada = data_nascimento.Year + "-" + data_nascimento.Month + "-" + data_nascimento.Day;
            string cadastro_insert = "INSERT INTO Trabalho_G2_Desktop_II.clientes (nome_cliente, genero, endereco, saldo, data_nascimento) VALUES ('" + cliente.Nome_cliente + "', '" + cliente.Genero + "', '" + cliente.Endereco + "', " + cliente.Saldo + ", '" + data_formatada + "');";

            if (cliente.Nome_cliente == null || cliente.Genero == null || cliente.Endereco == null)
            {
                MessageBox.Show("Dados obrigatórios não preenchidos!");
            }

            else
            {
                try
                {
                    conexao.Open();
                    MySqlCommand query_cadastro = new MySqlCommand(cadastro_insert, conexao);
                    query_cadastro.ExecuteNonQuery();

                    MessageBox.Show("Informações cadastradas com sucesso!");
                }

                catch (MySqlException ex)
                {
                    MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
                }

                catch (Exception ex)
                {
                    MessageBox.Show(" Erro: " + ex.Message);
                }

                finally
                {
                    conexao.Close();

                    textBox_nome.Clear();
                    textBox_endereco.Clear();
                    num_saldo.Value = 0;
                    date_data.SendToBack();
                }
            }
        }
    }
}
