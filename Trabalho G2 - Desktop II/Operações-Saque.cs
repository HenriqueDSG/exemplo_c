﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{
    public partial class Operacoes_saque : Form
    {
        MySqlConnection conexao;
        Clientes cliente = new Clientes();

        public Operacoes_saque()
        {
            InitializeComponent();
            conexao = new MySqlConnection("Server = localhost; Database = Trabalho_G2_Desktop_II; port = 3306; username = root; password = ");
            WindowState = FormWindowState.Maximized;
        }

        private void button_confirmar_Click(object sender, EventArgs e)
        {
            cliente.Id_cliente = Convert.ToInt32(num_id.Value);
            decimal saque = num_saque.Value;
            string saque_update = "UPDATE clientes SET saldo = saldo - " + saque + " WHERE id_cliente = " + cliente.Id_cliente + ";";
            string busca_id = "SELECT id_cliente, nome_cliente, saldo FROM clientes WHERE id_cliente = " + cliente.Id_cliente + ";";

            if (saque <= 0)
            {
                MessageBox.Show("Valor inválido!");
                num_id.Value = 0;
                num_saque.Value = 0;
            }

            else
            {              
                try
                {
                    conexao.Open();
                    MySqlCommand query_busca = new MySqlCommand(busca_id, conexao);
                    MySqlDataReader resultado = query_busca.ExecuteReader();

                    if (resultado.HasRows)
                    {
                        if (saque > Convert.ToInt32(resultado[2]))
                        {
                            MessageBox.Show("Saque acima do valor de saldo da conta!");
                        }

                        else
                        {
                            resultado.Read();
                            DialogResult opcao = MessageBox.Show("Deseja fazer saque nessa conta? " +
                                "\r\n 'ID = " + resultado[0].ToString() +
                                "\r\n Nome = " + resultado[1].ToString() +
                                "\r\n Saldo = " + resultado[2] + "'",
                                "Saque", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            conexao.Close();

                            if (opcao == DialogResult.Yes)
                            {
                                conexao.Open();
                                MySqlCommand query_saque = new MySqlCommand(saque_update, conexao);
                                query_saque.ExecuteNonQuery();

                                MessageBox.Show("Saque realizado com sucesso!");
                            }
                        }
                    }

                    else
                    {
                        MessageBox.Show("Nenhum cliente cadastrado com o ID fornecido");
                    }

                    if (resultado != null)
                    {
                        resultado.Close();
                    }
                }

                catch (MySqlException ex)
                {
                    MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
                }

                catch (Exception ex)
                {
                    MessageBox.Show(" Erro: " + ex.Message);
                }

                finally
                {
                    conexao.Close();
                    num_id.Value = 0;
                    num_saque.Value = 0;
                }
            }
        }

        private void button_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult opcao = MessageBox.Show("Deseja fechar a janela? ", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (opcao == DialogResult.Yes)
            {
                num_id.Value = 0;
                num_saque.Value = 0;
                Close();
            }
        }
    }
}
