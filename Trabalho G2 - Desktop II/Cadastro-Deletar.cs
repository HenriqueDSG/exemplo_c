﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{
    public partial class Cadastro_Deletar : Form
    {
        MySqlConnection conexao;
        Clientes cliente = new Clientes();

        public Cadastro_Deletar()
        {
            InitializeComponent();
            conexao = new MySqlConnection("Server = localhost; Database = Trabalho_G2_Desktop_II; port = 3306; username = root; password = ");
            WindowState = FormWindowState.Maximized;
        }

        private void button_confirmar_Click(object sender, EventArgs e)
        {              
            cliente.Id_cliente = Convert.ToInt32(num_id.Value);
            string deletar_delete = "DELETE FROM clientes WHERE id_cliente = " + cliente.Id_cliente + ";";
            string busca_select = "SELECT * FROM clientes WHERE id_cliente = " + cliente.Id_cliente + ";";

            try
            {
                conexao.Open();                                     
                MySqlCommand query_busca = new MySqlCommand(busca_select, conexao);
                MySqlDataReader resultado = query_busca.ExecuteReader();

                if (resultado.HasRows)
                {
                    resultado.Read();

                    DialogResult opcao = MessageBox.Show("Deseja apagar esse cliente?" +
                        "\r\n 'ID = " + resultado[0].ToString() +
                        "\r\n Nome = " + resultado[1].ToString() +
                        "\r\n Data de nascimento = " + resultado[2].ToString() +
                        "\r\n Genero = " + resultado[3].ToString() +
                        "\r\n Endereço = " + resultado[4].ToString() +
                        "\r\n Saldo = R$ " + resultado[5].ToString()
                        , "Apagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    conexao.Close();

                    if (opcao == DialogResult.Yes)
                    {
                        conexao.Open();
                        MySqlCommand query_deletar = new MySqlCommand(deletar_delete, conexao);
                        query_deletar.ExecuteNonQuery();

                        MessageBox.Show("Cliente apagado com sucesso!");
                    }

                }

                else
                {
                    MessageBox.Show("Nenhum cliente cadastrado com o ID fornecido");
                }

                if (resultado != null)
                {
                    resultado.Close();
                }
            }

            catch (MySqlException ex)
            {
                MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
            }

            catch (Exception ex)
            {
                MessageBox.Show(" Erro: " + ex.Message);
            }

            finally
            {
                conexao.Close();
                num_id.Value = 0;
            }
        }

        private void button_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult opcao = MessageBox.Show("Deseja fechar a janela? ", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (opcao == DialogResult.Yes)
            {
                Close();
            }
        }
    }
}
