﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{
    public partial class Relatório_Todos_os_Clientes : Form
    {
        MySqlConnection conexao;

        public Relatório_Todos_os_Clientes()
        {
            InitializeComponent();
            conexao = new MySqlConnection("Server = localhost; Database = Trabalho_G2_Desktop_II; port = 3306; username = root; password = ");
            WindowState = FormWindowState.Maximized;

            string busca_relatorio = "SELECT * FROM clientes;";

            try
            {
                conexao.Open();
                MySqlCommand query_relatorio = new MySqlCommand(busca_relatorio, conexao);
                MySqlDataReader resultado_relatorio = query_relatorio.ExecuteReader();

                if (resultado_relatorio.HasRows)
                {
                    while (resultado_relatorio.Read())
                    {
                        textBox_informacoes.Text +=
                            "ID Cliente = " + resultado_relatorio[0].ToString() +
                            "\r\n Nome Cliente = " + resultado_relatorio[1].ToString() +
                            "\r\n Data de Nascimento = " + resultado_relatorio[2].ToString() +
                            "\r\n Gênero = " + resultado_relatorio[3].ToString() +
                            "\r\n Endereço = " + resultado_relatorio[4].ToString() +
                            "\r\n Saldo = R$ " + resultado_relatorio[5].ToString() + "\r\n\r\n\r\n";
                    }
                }

                else
                {
                    textBox_informacoes.Text = "Nenhum cliente cadastrado";
                }

                if (resultado_relatorio != null)
                {
                    resultado_relatorio.Close();
                }
            }

            catch (MySqlException ex)
            {
                MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
            }

            catch (Exception ex)
            {
                MessageBox.Show(" Erro: " + ex.Message);
            }

            finally
            {
                conexao.Close();
            }
        }

        private void button_fechar_Click(object sender, EventArgs e)
        {
            DialogResult opcao = MessageBox.Show("Deseja fechar a janela? ", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (opcao == DialogResult.Yes)
            {
                Close();
            }
        }
    }
}
