﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{
    public partial class Cadastro_Atualizar : Form
    {
        MySqlConnection conexao;
        Clientes cliente = new Clientes();

        public Cadastro_Atualizar()
        {
            InitializeComponent();
            conexao = new MySqlConnection("Server = localhost; Database = Trabalho_G2_Desktop_II; port = 3306; username = root; password = ");
            WindowState = FormWindowState.Maximized;          
        }

        private void button_confirmar_Click(object sender, EventArgs e)
        {           
            cliente.Id_cliente = Convert.ToInt32(num_id.Value);
            string busca_id = "SELECT * FROM clientes WHERE id_cliente = " + cliente.Id_cliente + ";";                       

            try
            {
                conexao.Open();
                MySqlCommand query_busca_id = new MySqlCommand(busca_id, conexao);
                MySqlDataReader resultado = query_busca_id.ExecuteReader();

                if (resultado.HasRows)
                {
                    resultado.Read();

                    textBox_nome.Enabled = true;
                    textBox_endereco.Enabled = true;
                    num_saldo.Enabled = true;
                    date_data.Enabled = true;
                    comboBox_genero.Enabled = true;
                    button_atualizar.Enabled = true;
                    button_cancelar.Enabled = true;

                    textBox_nome.Text = resultado[1].ToString();
                    textBox_endereco.Text = resultado[4].ToString();
                    num_saldo.Value = Convert.ToDecimal(resultado[5]);
                    comboBox_genero.SelectedItem = resultado[3].ToString();
                    date_data.Value = Convert.ToDateTime(resultado[2]);
                    textBox_id.Text = resultado[0].ToString();
                }

                else
                {
                    MessageBox.Show("Nenhum cliente cadastrado com o ID fornecido!");
                    textBox_id.Clear();
                }

                if (resultado != null)
                {
                    resultado.Close();
                }
            }

            catch (MySqlException ex)
            {
                MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
            }

            catch (Exception ex)
            {
                MessageBox.Show(" Erro: " + ex.Message);
            }

            finally
            {                   
                conexao.Close();                  
            }
        }

        private void button_atualizar_Click(object sender, EventArgs e)
        {
            cliente.Nome_cliente = textBox_nome.Text;
            cliente.Endereco = textBox_endereco.Text;
            cliente.Genero = comboBox_genero.SelectedText.ToString();
            cliente.Saldo = num_saldo.Value;

            DateTime data_nascimento = date_data.Value;
            string data_formatada = data_nascimento.Year + "-" + data_nascimento.Month + "-" + data_nascimento.Day;

            string atualizar = "UPDATE Trabalho_G2_Desktop_II.clientes SET nome_cliente = " + 
                cliente.Nome_cliente + ", endereco = " + 
                cliente.Endereco + ", genero = " + 
                cliente.Genero + ", saldo = " + 
                cliente.Saldo + ", data_nascimento = " + 
                data_formatada + " WHERE id_cliente = " + 
                cliente.Id_cliente + ";";

            try
            {
                conexao.Open();
                MySqlCommand query_atualizar = new MySqlCommand(atualizar, conexao);
                query_atualizar.ExecuteNonQuery();

                MessageBox.Show("Dados atualizados com sucesso!");
            }

            catch (MySqlException ex)
            {
                MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
            }

            catch (Exception ex)
            {
                MessageBox.Show(" Erro: " + ex.Message);
            }

            finally
            {
                conexao.Close();

                textBox_nome.Clear();
                textBox_endereco.Clear();
                num_saldo.Value = 0;
                date_data.SendToBack();
                textBox_id.Clear();
            }
        }

        private void button_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult opcao = MessageBox.Show("Deseja fechar a janela? ", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (opcao == DialogResult.Yes)
            {
                Close();
            }
        }
    }
}
