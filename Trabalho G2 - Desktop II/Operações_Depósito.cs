﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{
    public partial class Operações_Depósito : Form
    {
        MySqlConnection conexao;
        Clientes cliente = new Clientes();

        public Operações_Depósito()
        {
            InitializeComponent();
            conexao = new MySqlConnection("Server = localhost; Database = Trabalho_G2_Desktop_II; port = 3306; username = root; password = ");
            WindowState = FormWindowState.Maximized;
        }

        private void button_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult opcao = MessageBox.Show("Deseja fechar a janela? ", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (opcao == DialogResult.Yes)
            {
                Close();
            }
        }

        private void button_confirmar_Click(object sender, EventArgs e)
        {
            cliente.Id_cliente = Convert.ToInt32(num_id.Value);
            decimal deposito = num_deposito.Value;
            string deposito_update = "UPDATE clientes SET saldo = saldo + " + deposito + " WHERE id_cliente = " + cliente.Id_cliente + ";";
            string busca_id = "SELECT nome_cliente FROM clientes WHERE id_cliente = " + cliente.Id_cliente + ";";

            if (deposito <= 0)
            {
                MessageBox.Show("Valor inválido!");
            }

            else
            {
                try
                {
                    conexao.Open();
                    MySqlCommand query_busca = new MySqlCommand(busca_id, conexao);
                    MySqlDataReader resultado = query_busca.ExecuteReader();

                    if (resultado.HasRows)
                    {
                        resultado.Read();

                        DialogResult opcao = MessageBox.Show("Deseja fazer um deposito para essa conta? " +
                            "\r\n 'ID do Cliente= " + cliente.Id_cliente +
                            "\r\n Nome do Cliente = " + resultado[0].ToString() + "'",
                            "Depósito", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        conexao.Close();
                        
                        if (opcao == DialogResult.Yes)
                        {
                            conexao.Open();
                            MySqlCommand query_deposito = new MySqlCommand(deposito_update, conexao);
                            query_deposito.ExecuteNonQuery();

                            MessageBox.Show("Depósito realizado com sucesso!");
                        }                    
                    }
                    
                    else
                    {
                        MessageBox.Show("Nenhum cliente cadastrado com o ID fornecido!");
                    }

                    if (resultado != null)
                    {
                        resultado.Close();
                    }
                }

                catch (MySqlException ex)
                {
                    MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
                }

                catch (Exception ex)
                {
                    MessageBox.Show(" Erro: " + ex.Message);
                }

                finally
                {
                    conexao.Close();                   
                }
            }             
        }
    }
}
