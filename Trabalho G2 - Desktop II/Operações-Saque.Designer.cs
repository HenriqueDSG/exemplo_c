﻿namespace Trabalho_G2___Desktop_II
{
    partial class Operacoes_saque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.num_saque = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.button_cancelar = new System.Windows.Forms.Button();
            this.num_id = new System.Windows.Forms.NumericUpDown();
            this.button_confirmar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.num_saque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).BeginInit();
            this.SuspendLayout();
            // 
            // num_saque
            // 
            this.num_saque.DecimalPlaces = 2;
            this.num_saque.Location = new System.Drawing.Point(350, 72);
            this.num_saque.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.num_saque.Name = "num_saque";
            this.num_saque.Size = new System.Drawing.Size(125, 20);
            this.num_saque.TabIndex = 36;
            this.num_saque.ThousandsSeparator = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(347, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Saque: R$";
            // 
            // button_cancelar
            // 
            this.button_cancelar.Location = new System.Drawing.Point(319, 98);
            this.button_cancelar.Name = "button_cancelar";
            this.button_cancelar.Size = new System.Drawing.Size(75, 23);
            this.button_cancelar.TabIndex = 34;
            this.button_cancelar.Text = "Fechar";
            this.button_cancelar.UseVisualStyleBackColor = true;
            this.button_cancelar.Click += new System.EventHandler(this.button_cancelar_Click);
            // 
            // num_id
            // 
            this.num_id.Location = new System.Drawing.Point(12, 72);
            this.num_id.Name = "num_id";
            this.num_id.Size = new System.Drawing.Size(120, 20);
            this.num_id.TabIndex = 33;
            // 
            // button_confirmar
            // 
            this.button_confirmar.Location = new System.Drawing.Point(400, 98);
            this.button_confirmar.Name = "button_confirmar";
            this.button_confirmar.Size = new System.Drawing.Size(75, 23);
            this.button_confirmar.TabIndex = 32;
            this.button_confirmar.Text = "Confirmar";
            this.button_confirmar.UseVisualStyleBackColor = true;
            this.button_confirmar.Click += new System.EventHandler(this.button_confirmar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Digite o código do cliente desejado:";
            // 
            // Operacoes_saque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.num_saque);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_cancelar);
            this.Controls.Add(this.num_id);
            this.Controls.Add(this.button_confirmar);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Operacoes_saque";
            this.Text = "Saque";
            ((System.ComponentModel.ISupportInitialize)(this.num_saque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown num_saque;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_cancelar;
        private System.Windows.Forms.NumericUpDown num_id;
        private System.Windows.Forms.Button button_confirmar;
        private System.Windows.Forms.Label label1;
    }
}