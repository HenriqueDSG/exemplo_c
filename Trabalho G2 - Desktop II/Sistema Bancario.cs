﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trabalho_G2___Desktop_II
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_Novo janela_cadastro_novo = new Cadastro_Novo { MdiParent = this };
            janela_cadastro_novo.Show();
            janela_cadastro_novo.Location = new Point(0, 0);
        }

        private void atualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_Atualizar janela_cadastro_atualizar = new Cadastro_Atualizar { MdiParent = this };
            janela_cadastro_atualizar.Show();
            janela_cadastro_atualizar.Location = new Point(0, 0);
        }

        private void deletarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_Deletar janela_cadastro_deletar = new Cadastro_Deletar { MdiParent = this };
            janela_cadastro_deletar.Show();
            janela_cadastro_deletar.Location = new Point(0, 0);
        }

        private void depósitoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Operações_Depósito janela_operacoes_deposito = new Operações_Depósito { MdiParent = this };
            janela_operacoes_deposito.Show();
            janela_operacoes_deposito.Location = new Point(0, 0);
        }

        private void saqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Operacoes_saque janela_operacoes_saque = new Operacoes_saque { MdiParent = this };
            janela_operacoes_saque.Show();
            janela_operacoes_saque.Location = new Point(0, 0);
        }

        private void saldoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Operação_Saldo janela_operacoes_saldo = new Operação_Saldo { MdiParent = this };
            janela_operacoes_saldo.Show();
            janela_operacoes_saldo.Location = new Point(0, 0);
        }

        private void transferênciaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Operações_Transferência janela_operacoes_transferencia = new Operações_Transferência { MdiParent = this };
            janela_operacoes_transferencia.Show();
            janela_operacoes_transferencia.Location = new Point(0, 0);
        }

        private void umClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Relatório_Um_Cliente janela_relatorio_um_cliente = new Relatório_Um_Cliente { MdiParent = this };
            janela_relatorio_um_cliente.Show();
            janela_relatorio_um_cliente.Location = new Point(0, 0);
        }

        private void todosOsClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Relatório_Todos_os_Clientes janela_relatorio_todos_clientes = new Relatório_Todos_os_Clientes { MdiParent = this };
            janela_relatorio_todos_clientes.Show();
            janela_relatorio_todos_clientes.Location = new Point(0, 0);
        }
    }
}
