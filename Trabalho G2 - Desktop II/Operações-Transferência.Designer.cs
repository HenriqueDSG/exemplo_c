﻿namespace Trabalho_G2___Desktop_II
{
    partial class Operações_Transferência
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.num_transferencia = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.button_cancelar = new System.Windows.Forms.Button();
            this.num_id_saque = new System.Windows.Forms.NumericUpDown();
            this.button_confirmar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.num_id_deposito = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.num_transferencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id_saque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id_deposito)).BeginInit();
            this.SuspendLayout();
            // 
            // num_transferencia
            // 
            this.num_transferencia.DecimalPlaces = 2;
            this.num_transferencia.Location = new System.Drawing.Point(350, 72);
            this.num_transferencia.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.num_transferencia.Name = "num_transferencia";
            this.num_transferencia.Size = new System.Drawing.Size(125, 20);
            this.num_transferencia.TabIndex = 36;
            this.num_transferencia.ThousandsSeparator = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(347, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Transferência: R$";
            // 
            // button_cancelar
            // 
            this.button_cancelar.Location = new System.Drawing.Point(319, 98);
            this.button_cancelar.Name = "button_cancelar";
            this.button_cancelar.Size = new System.Drawing.Size(75, 23);
            this.button_cancelar.TabIndex = 34;
            this.button_cancelar.Text = "Fechar";
            this.button_cancelar.UseVisualStyleBackColor = true;
            this.button_cancelar.Click += new System.EventHandler(this.button_cancelar_Click);
            // 
            // num_id_saque
            // 
            this.num_id_saque.Location = new System.Drawing.Point(12, 72);
            this.num_id_saque.Name = "num_id_saque";
            this.num_id_saque.Size = new System.Drawing.Size(120, 20);
            this.num_id_saque.TabIndex = 33;
            // 
            // button_confirmar
            // 
            this.button_confirmar.Location = new System.Drawing.Point(400, 98);
            this.button_confirmar.Name = "button_confirmar";
            this.button_confirmar.Size = new System.Drawing.Size(75, 23);
            this.button_confirmar.TabIndex = 32;
            this.button_confirmar.Text = "Confirmar";
            this.button_confirmar.UseVisualStyleBackColor = true;
            this.button_confirmar.Click += new System.EventHandler(this.button_confirmar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Digite o ID do Cliente a ser sacado:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Digite o ID do cliente a ser depositado:";
            // 
            // num_id_deposito
            // 
            this.num_id_deposito.Location = new System.Drawing.Point(12, 124);
            this.num_id_deposito.Name = "num_id_deposito";
            this.num_id_deposito.Size = new System.Drawing.Size(120, 20);
            this.num_id_deposito.TabIndex = 33;
            // 
            // Operações_Transferência
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.num_transferencia);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_cancelar);
            this.Controls.Add(this.num_id_deposito);
            this.Controls.Add(this.num_id_saque);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_confirmar);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Operações_Transferência";
            this.Text = "Operações_Transferência";
            ((System.ComponentModel.ISupportInitialize)(this.num_transferencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id_saque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id_deposito)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown num_transferencia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_cancelar;
        private System.Windows.Forms.NumericUpDown num_id_saque;
        private System.Windows.Forms.Button button_confirmar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown num_id_deposito;
    }
}