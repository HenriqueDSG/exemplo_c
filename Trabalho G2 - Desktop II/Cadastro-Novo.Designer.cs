﻿namespace Trabalho_G2___Desktop_II
{
    partial class Cadastro_Novo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_fechar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_nome = new System.Windows.Forms.TextBox();
            this.num_saldo = new System.Windows.Forms.NumericUpDown();
            this.textBox_endereco = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button_salvar = new System.Windows.Forms.Button();
            this.comboBox_genero = new System.Windows.Forms.ComboBox();
            this.date_data = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.num_saldo)).BeginInit();
            this.SuspendLayout();
            // 
            // button_fechar
            // 
            this.button_fechar.Location = new System.Drawing.Point(360, 203);
            this.button_fechar.Name = "button_fechar";
            this.button_fechar.Size = new System.Drawing.Size(75, 23);
            this.button_fechar.TabIndex = 0;
            this.button_fechar.Text = "Fechar";
            this.button_fechar.UseVisualStyleBackColor = true;
            this.button_fechar.Click += new System.EventHandler(this.button_fechar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome:";
            // 
            // textBox_nome
            // 
            this.textBox_nome.Location = new System.Drawing.Point(125, 60);
            this.textBox_nome.Name = "textBox_nome";
            this.textBox_nome.Size = new System.Drawing.Size(220, 20);
            this.textBox_nome.TabIndex = 2;
            // 
            // num_saldo
            // 
            this.num_saldo.DecimalPlaces = 2;
            this.num_saldo.Location = new System.Drawing.Point(125, 201);
            this.num_saldo.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.num_saldo.Name = "num_saldo";
            this.num_saldo.Size = new System.Drawing.Size(125, 20);
            this.num_saldo.TabIndex = 3;
            this.num_saldo.ThousandsSeparator = true;
            // 
            // textBox_endereco
            // 
            this.textBox_endereco.Location = new System.Drawing.Point(125, 164);
            this.textBox_endereco.Name = "textBox_endereco";
            this.textBox_endereco.Size = new System.Drawing.Size(391, 20);
            this.textBox_endereco.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Gênero:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Saldo: R$";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Endereço";
            // 
            // button_salvar
            // 
            this.button_salvar.Location = new System.Drawing.Point(441, 203);
            this.button_salvar.Name = "button_salvar";
            this.button_salvar.Size = new System.Drawing.Size(75, 23);
            this.button_salvar.TabIndex = 0;
            this.button_salvar.Text = "Confirmar";
            this.button_salvar.UseVisualStyleBackColor = true;
            this.button_salvar.Click += new System.EventHandler(this.button_salvar_Click);
            // 
            // comboBox_genero
            // 
            this.comboBox_genero.FormattingEnabled = true;
            this.comboBox_genero.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.comboBox_genero.Location = new System.Drawing.Point(125, 125);
            this.comboBox_genero.Name = "comboBox_genero";
            this.comboBox_genero.Size = new System.Drawing.Size(176, 21);
            this.comboBox_genero.TabIndex = 5;
            // 
            // date_data
            // 
            this.date_data.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date_data.Location = new System.Drawing.Point(125, 92);
            this.date_data.Name = "date_data";
            this.date_data.Size = new System.Drawing.Size(87, 20);
            this.date_data.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Data de Nascimento:";
            // 
            // Cadastro_Novo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(608, 306);
            this.Controls.Add(this.date_data);
            this.Controls.Add(this.comboBox_genero);
            this.Controls.Add(this.num_saldo);
            this.Controls.Add(this.textBox_endereco);
            this.Controls.Add(this.textBox_nome);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_salvar);
            this.Controls.Add(this.button_fechar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Cadastro_Novo";
            this.Text = "Cadastro Novo Cliente";
            ((System.ComponentModel.ISupportInitialize)(this.num_saldo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_fechar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_nome;
        private System.Windows.Forms.NumericUpDown num_saldo;
        private System.Windows.Forms.TextBox textBox_endereco;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button_salvar;
        private System.Windows.Forms.ComboBox comboBox_genero;
        private System.Windows.Forms.DateTimePicker date_data;
        private System.Windows.Forms.Label label4;
    }
}