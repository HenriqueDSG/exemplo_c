﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{
    class Clientes
    {
        string nome_cliente, genero, endereco;
        decimal saldo = 0;
        int id_cliente;        

        public string Nome_cliente { get => nome_cliente; set => nome_cliente = value; }
        public string Genero { get => genero; set => genero = value; }
        public string Endereco { get => endereco; set => endereco = value; }
        public decimal Saldo { get => saldo; set => saldo = value; }
        public int Id_cliente { get => id_cliente; set => id_cliente = value; }    
    }
}
