﻿namespace Trabalho_G2___Desktop_II
{
    partial class Operação_Saldo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_cancelar = new System.Windows.Forms.Button();
            this.num_id = new System.Windows.Forms.NumericUpDown();
            this.button_confirmar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).BeginInit();
            this.SuspendLayout();
            // 
            // button_cancelar
            // 
            this.button_cancelar.Location = new System.Drawing.Point(12, 98);
            this.button_cancelar.Name = "button_cancelar";
            this.button_cancelar.Size = new System.Drawing.Size(75, 23);
            this.button_cancelar.TabIndex = 40;
            this.button_cancelar.Text = "Fechar";
            this.button_cancelar.UseVisualStyleBackColor = true;
            this.button_cancelar.Click += new System.EventHandler(this.button_cancelar_Click);
            // 
            // num_id
            // 
            this.num_id.Location = new System.Drawing.Point(12, 72);
            this.num_id.Name = "num_id";
            this.num_id.Size = new System.Drawing.Size(120, 20);
            this.num_id.TabIndex = 39;
            // 
            // button_confirmar
            // 
            this.button_confirmar.Location = new System.Drawing.Point(93, 98);
            this.button_confirmar.Name = "button_confirmar";
            this.button_confirmar.Size = new System.Drawing.Size(75, 23);
            this.button_confirmar.TabIndex = 38;
            this.button_confirmar.Text = "Confirmar";
            this.button_confirmar.UseVisualStyleBackColor = true;
            this.button_confirmar.Click += new System.EventHandler(this.button_confirmar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Digite o código do cliente desejado:";
            // 
            // Operação_Saldo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(469, 427);
            this.Controls.Add(this.button_cancelar);
            this.Controls.Add(this.num_id);
            this.Controls.Add(this.button_confirmar);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Operação_Saldo";
            this.Text = "Operação_Saldo";
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_cancelar;
        private System.Windows.Forms.NumericUpDown num_id;
        private System.Windows.Forms.Button button_confirmar;
        private System.Windows.Forms.Label label1;
    }
}