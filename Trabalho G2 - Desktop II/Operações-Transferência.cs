﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{
    public partial class Operações_Transferência : Form
    {
        MySqlConnection conexao;

        public Operações_Transferência()
        {
            InitializeComponent();
            conexao = new MySqlConnection("Server = localhost; Database = Trabalho_G2_Desktop_II; port = 3306; username = root; password = ");
            WindowState = FormWindowState.Maximized;
        }

        private void button_confirmar_Click(object sender, EventArgs e)
        {
            int id_saque = Convert.ToInt32(num_id_saque.Value);
            int id_deposito = Convert.ToInt32(num_id_deposito.Value);
            decimal transferencia = num_transferencia.Value;

            string busca_saldo = "SELECT saldo FROM clientes WHERE id_cliente = " + id_saque + ";";
            string busca_deposito = "SELECT id_cliente, nome_cliente FROM clientes WHERE id_cliente = " + id_deposito + ";";

            string saque = "UPDATE clientes SET saldo = saldo - " + transferencia + " WHERE id_cliente = " + id_saque + ";";
            string deposito = "UPDATE clientes SET saldo = saldo + " + transferencia + " WHERE id_cliente = " + id_deposito + ";";

            if (transferencia <= 0)
            {
                MessageBox.Show("Valor inválido!");
            }

            else
            {
                try
                {
                    conexao.Open();
                    MySqlCommand query_busca_deposito = new MySqlCommand(busca_deposito, conexao);
                    MySqlDataReader resultado_deposito = query_busca_deposito.ExecuteReader();

                    if (resultado_deposito.HasRows)
                    {
                        resultado_deposito.Read();
                        int id_aux = Convert.ToInt32(resultado_deposito[0].ToString());
                        string nome_aux = resultado_deposito[1].ToString();
                        conexao.Close();

                        conexao.Open();
                        MySqlCommand query_busca_saldo = new MySqlCommand(busca_saldo, conexao);
                        MySqlDataReader resultado_saldo = query_busca_saldo.ExecuteReader();

                        if (resultado_saldo.HasRows)
                        {
                            if (transferencia > Convert.ToDecimal(resultado_saldo[0]))
                            {
                                MessageBox.Show("Saque acima do valor de saldo da conta!");
                            }

                            else
                            {
                                resultado_saldo.Read();
                                DialogResult opcao = MessageBox.Show("Deseja fazer transferência para essa conta? " +
                                    "\r\n 'ID = " + id_aux +
                                    "\r\n Nome = " + nome_aux + "'" +
                                    "\r\n\r\n Saldo disponivel = " + resultado_saldo[0].ToString(),
                                    "Transferência", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                conexao.Close();

                                if (opcao == DialogResult.Yes)
                                {
                                    conexao.Open();
                                    MySqlCommand query_saque = new MySqlCommand(saque, conexao);
                                    MySqlCommand query_deposito = new MySqlCommand(deposito, conexao);
                                    query_saque.ExecuteNonQuery();

                                    MessageBox.Show("Transferência realizada com sucesso!");
                                }
                            }
                        }

                        else
                        {
                            MessageBox.Show("Nenhum cliente cadastrado com o ID fornecido!");
                        }

                        if (resultado_saldo != null)
                        {
                            resultado_saldo.Close();
                        }
                    }

                    else
                    {
                        MessageBox.Show("Nenhum cliente cadastrado com o ID fornecido!");
                    }

                    if (resultado_deposito != null)
                    {
                        resultado_deposito.Close();
                    }
                }

                catch (MySqlException ex)
                {
                    MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
                }

                catch (Exception ex)
                {
                    MessageBox.Show(" Erro: " + ex.Message);
                }

                finally
                {
                    conexao.Close();
                    num_id_deposito.Value = 0;
                    num_id_saque.Value = 0;
                    num_transferencia.Value = 0;
                }
            }
        }

        private void button_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult opcao = MessageBox.Show("Deseja fechar a janela? ", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (opcao == DialogResult.Yes)
            {
                Close();
                num_id_deposito.Value = 0;
                num_id_saque.Value = 0;
                num_transferencia.Value = 0;
            }
        }
    }
}
