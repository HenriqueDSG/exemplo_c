﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{   
    public partial class Operação_Saldo : Form
    {
        MySqlConnection conexao;
        Clientes cliente = new Clientes();

        public Operação_Saldo()
        {
            InitializeComponent();
            conexao = new MySqlConnection("Server = localhost; Database = Trabalho_G2_Desktop_II; port = 3306; username = root; password = ");
            WindowState = FormWindowState.Maximized;
        }

        private void button_confirmar_Click(object sender, EventArgs e)
        {
            cliente.Id_cliente = Convert.ToInt32(num_id.Value);
            string busca_saldo = "SELECT nome_cliente, saldo FROM clientes WHERE id_cliente = " + cliente.Id_cliente + ";";
 
            try
            {
                conexao.Open();
                MySqlCommand query_saldo = new MySqlCommand(busca_saldo, conexao);                   
                MySqlDataReader resultado = query_saldo.ExecuteReader();

                if (resultado.HasRows)
                {
                    resultado.Read();
                    MessageBox.Show("O saldo disponivel para o cliente '" + 
                        resultado[0].ToString() + "': " +
                        "\r\n R$ " + resultado[1].ToString());
                }

                else
                {
                    MessageBox.Show("Nenhum cliente cadastrado com o ID fornecido");
                }

                if (resultado != null)
                {
                    resultado.Close();
                }

            }

            catch (MySqlException ex)
            {
                MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
            }

            catch (Exception ex)
            {
                MessageBox.Show(" Erro: " + ex.Message);
            }

            finally
            {
                conexao.Close();
                num_id.Value = 0;
            }           
        }

        private void button_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult opcao = MessageBox.Show("Deseja fechar a janela? ", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (opcao == DialogResult.Yes)
            {
                num_id.Value = 0;
                Close();
            }
        }
    }
}
