﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Trabalho_G2___Desktop_II
{
    public partial class Relatório_Um_Cliente : Form
    {
        MySqlConnection conexao;
        Clientes cliente = new Clientes();

        public Relatório_Um_Cliente()
        {
            InitializeComponent();
            conexao = new MySqlConnection("Server = localhost; Database = Trabalho_G2_Desktop_II; port = 3306; username = root; password = ");
            WindowState = FormWindowState.Maximized;
        }

        private void button_confirmar_Click(object sender, EventArgs e)
        {
            cliente.Id_cliente = Convert.ToInt32(num_id.Value);
            string busca_id = "SELECT * FROM clientes WHERE id_cliente =" + cliente.Id_cliente + ";";

            try
            {
                conexao.Open();
                MySqlCommand query_busca = new MySqlCommand(busca_id, conexao);
                MySqlDataReader resultado = query_busca.ExecuteReader();

                if (resultado.HasRows)
                {
                    resultado.Read();
                    textBox_informacoes.Clear();
                    textBox_informacoes.Text =
                        " ID = " + resultado[0].ToString() +
                        "\r\n Nome do Cliente = " + resultado[1].ToString() +
                        "\r\n Data de Nascimento = " + resultado[2].ToString() +
                        "\r\n Gênero = " + resultado[3].ToString() +
                        "\r\n Endereço = " + resultado[4].ToString() +
                        "\r\n Saldo = R$ " + resultado[5].ToString();
                }

                else
                {
                    MessageBox.Show("Nenhum cliente cadastrado com o ID fornecido");
                }

                if (resultado != null)
                {
                    resultado.Close();
                }
            }

            catch (MySqlException ex)
            {
                MessageBox.Show(" Erro (" + ex.Number + "): " + ex.Message);
            }

            catch (Exception ex)
            {
                MessageBox.Show(" Erro: " + ex.Message);
            }

            finally
            {
                num_id.Value = 0;
                conexao.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult opcao = MessageBox.Show("Deseja fechar a janela? ", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (opcao == DialogResult.Yes)
            {
                Close();
                num_id.Value = 0;
            }
        }
    }
}
