﻿namespace Trabalho_G2___Desktop_II
{
    partial class Relatório_Um_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.num_id = new System.Windows.Forms.NumericUpDown();
            this.button_confirmar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox_informacoes = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).BeginInit();
            this.SuspendLayout();
            // 
            // num_id
            // 
            this.num_id.Location = new System.Drawing.Point(324, 72);
            this.num_id.Name = "num_id";
            this.num_id.Size = new System.Drawing.Size(120, 20);
            this.num_id.TabIndex = 24;
            // 
            // button_confirmar
            // 
            this.button_confirmar.Location = new System.Drawing.Point(573, 72);
            this.button_confirmar.Name = "button_confirmar";
            this.button_confirmar.Size = new System.Drawing.Size(75, 23);
            this.button_confirmar.TabIndex = 23;
            this.button_confirmar.Text = "Confirmar";
            this.button_confirmar.UseVisualStyleBackColor = true;
            this.button_confirmar.Click += new System.EventHandler(this.button_confirmar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(321, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Digite o código do cliente desejado:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(492, 72);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "Fechar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox_informacoes
            // 
            this.textBox_informacoes.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_informacoes.Location = new System.Drawing.Point(12, 101);
            this.textBox_informacoes.Multiline = true;
            this.textBox_informacoes.Name = "textBox_informacoes";
            this.textBox_informacoes.ReadOnly = true;
            this.textBox_informacoes.Size = new System.Drawing.Size(636, 328);
            this.textBox_informacoes.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Relatório de Clientes:";
            // 
            // Relatório_Um_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(660, 618);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox_informacoes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.num_id);
            this.Controls.Add(this.button_confirmar);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Relatório_Um_Cliente";
            this.Text = "Relatórios";
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown num_id;
        private System.Windows.Forms.Button button_confirmar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox_informacoes;
        private System.Windows.Forms.Label label2;
    }
}