﻿namespace Trabalho_G2___Desktop_II
{
    partial class Cadastro_Atualizar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button_confirmar = new System.Windows.Forms.Button();
            this.date_data = new System.Windows.Forms.DateTimePicker();
            this.num_saldo = new System.Windows.Forms.NumericUpDown();
            this.textBox_endereco = new System.Windows.Forms.TextBox();
            this.textBox_nome = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button_atualizar = new System.Windows.Forms.Button();
            this.button_cancelar = new System.Windows.Forms.Button();
            this.num_id = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_id = new System.Windows.Forms.TextBox();
            this.comboBox_genero = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.num_saldo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Digite o código do cliente desejado:";
            // 
            // button_confirmar
            // 
            this.button_confirmar.Location = new System.Drawing.Point(12, 104);
            this.button_confirmar.Name = "button_confirmar";
            this.button_confirmar.Size = new System.Drawing.Size(75, 23);
            this.button_confirmar.TabIndex = 1;
            this.button_confirmar.Text = "Confirmar";
            this.button_confirmar.UseVisualStyleBackColor = true;
            this.button_confirmar.Click += new System.EventHandler(this.button_confirmar_Click);
            // 
            // date_data
            // 
            this.date_data.CustomFormat = "yyyy-MM-dd";
            this.date_data.Enabled = false;
            this.date_data.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_data.Location = new System.Drawing.Point(333, 82);
            this.date_data.Name = "date_data";
            this.date_data.Size = new System.Drawing.Size(105, 20);
            this.date_data.TabIndex = 16;
            // 
            // num_saldo
            // 
            this.num_saldo.DecimalPlaces = 2;
            this.num_saldo.Enabled = false;
            this.num_saldo.Location = new System.Drawing.Point(332, 180);
            this.num_saldo.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.num_saldo.Name = "num_saldo";
            this.num_saldo.Size = new System.Drawing.Size(125, 20);
            this.num_saldo.TabIndex = 15;
            this.num_saldo.ThousandsSeparator = true;
            // 
            // textBox_endereco
            // 
            this.textBox_endereco.Enabled = false;
            this.textBox_endereco.Location = new System.Drawing.Point(332, 148);
            this.textBox_endereco.Name = "textBox_endereco";
            this.textBox_endereco.Size = new System.Drawing.Size(391, 20);
            this.textBox_endereco.TabIndex = 13;
            // 
            // textBox_nome
            // 
            this.textBox_nome.Enabled = false;
            this.textBox_nome.Location = new System.Drawing.Point(332, 49);
            this.textBox_nome.Name = "textBox_nome";
            this.textBox_nome.Size = new System.Drawing.Size(220, 20);
            this.textBox_nome.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(219, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Endereço";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(219, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Data de Nascimento:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(219, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Saldo: R$";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(219, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Gênero:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(219, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Nome:";
            // 
            // button_atualizar
            // 
            this.button_atualizar.Enabled = false;
            this.button_atualizar.Location = new System.Drawing.Point(648, 182);
            this.button_atualizar.Name = "button_atualizar";
            this.button_atualizar.Size = new System.Drawing.Size(75, 23);
            this.button_atualizar.TabIndex = 6;
            this.button_atualizar.Text = "Atualizar";
            this.button_atualizar.UseVisualStyleBackColor = true;
            this.button_atualizar.Click += new System.EventHandler(this.button_atualizar_Click);
            // 
            // button_cancelar
            // 
            this.button_cancelar.Enabled = false;
            this.button_cancelar.Location = new System.Drawing.Point(567, 182);
            this.button_cancelar.Name = "button_cancelar";
            this.button_cancelar.Size = new System.Drawing.Size(75, 23);
            this.button_cancelar.TabIndex = 7;
            this.button_cancelar.Text = "Fechar";
            this.button_cancelar.UseVisualStyleBackColor = true;
            this.button_cancelar.Click += new System.EventHandler(this.button_cancelar_Click);
            // 
            // num_id
            // 
            this.num_id.Location = new System.Drawing.Point(12, 72);
            this.num_id.Name = "num_id";
            this.num_id.Size = new System.Drawing.Size(120, 20);
            this.num_id.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(470, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "ID:";
            // 
            // textBox_id
            // 
            this.textBox_id.Enabled = false;
            this.textBox_id.Location = new System.Drawing.Point(497, 81);
            this.textBox_id.Name = "textBox_id";
            this.textBox_id.ReadOnly = true;
            this.textBox_id.Size = new System.Drawing.Size(55, 20);
            this.textBox_id.TabIndex = 14;
            // 
            // comboBox_genero
            // 
            this.comboBox_genero.Enabled = false;
            this.comboBox_genero.FormattingEnabled = true;
            this.comboBox_genero.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.comboBox_genero.Location = new System.Drawing.Point(332, 114);
            this.comboBox_genero.Name = "comboBox_genero";
            this.comboBox_genero.Size = new System.Drawing.Size(220, 21);
            this.comboBox_genero.TabIndex = 17;
            // 
            // Cadastro_Atualizar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(745, 412);
            this.Controls.Add(this.num_id);
            this.Controls.Add(this.comboBox_genero);
            this.Controls.Add(this.date_data);
            this.Controls.Add(this.num_saldo);
            this.Controls.Add(this.textBox_endereco);
            this.Controls.Add(this.textBox_id);
            this.Controls.Add(this.textBox_nome);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button_atualizar);
            this.Controls.Add(this.button_cancelar);
            this.Controls.Add(this.button_confirmar);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Cadastro_Atualizar";
            this.Text = "Cadastro - Atualizar";
            ((System.ComponentModel.ISupportInitialize)(this.num_saldo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_confirmar;
        private System.Windows.Forms.DateTimePicker date_data;
        private System.Windows.Forms.NumericUpDown num_saldo;
        private System.Windows.Forms.TextBox textBox_endereco;
        private System.Windows.Forms.TextBox textBox_nome;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button_atualizar;
        private System.Windows.Forms.Button button_cancelar;
        private System.Windows.Forms.NumericUpDown num_id;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_id;
        private System.Windows.Forms.ComboBox comboBox_genero;
    }
}