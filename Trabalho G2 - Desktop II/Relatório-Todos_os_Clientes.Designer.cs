﻿namespace Trabalho_G2___Desktop_II
{
    partial class Relatório_Todos_os_Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_informacoes = new System.Windows.Forms.TextBox();
            this.button_fechar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Relatório de Clientes:";
            // 
            // textBox_informacoes
            // 
            this.textBox_informacoes.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_informacoes.Location = new System.Drawing.Point(12, 101);
            this.textBox_informacoes.Multiline = true;
            this.textBox_informacoes.Name = "textBox_informacoes";
            this.textBox_informacoes.ReadOnly = true;
            this.textBox_informacoes.Size = new System.Drawing.Size(636, 328);
            this.textBox_informacoes.TabIndex = 26;
            // 
            // button_fechar
            // 
            this.button_fechar.Location = new System.Drawing.Point(573, 72);
            this.button_fechar.Name = "button_fechar";
            this.button_fechar.Size = new System.Drawing.Size(75, 23);
            this.button_fechar.TabIndex = 27;
            this.button_fechar.Text = "Fechar";
            this.button_fechar.UseVisualStyleBackColor = true;
            this.button_fechar.Click += new System.EventHandler(this.button_fechar_Click);
            // 
            // Relatório_Todos_os_Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(660, 618);
            this.Controls.Add(this.button_fechar);
            this.Controls.Add(this.textBox_informacoes);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Relatório_Todos_os_Clientes";
            this.Text = "Relatório_Todos_os_Clientes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_informacoes;
        private System.Windows.Forms.Button button_fechar;
    }
}